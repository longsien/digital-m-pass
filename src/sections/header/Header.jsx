import React from 'react'
import logo from 'img/Monash_Reversed.png'
import phone from 'img/phone.png'
import watch from 'img/watch.png'
import reference from 'img/reference-image.jpg'
import block1 from 'img/block-1.jpg'
import block2 from 'img/block-2.jpg'
import block3 from 'img/block-3.jpg'
import pink from 'img/pink.png'
import green from 'img/green.png'
import purple from 'img/purple.png'
import c from './header.module.scss'

const Header = () => {
  // render

  return (
    <div className={c.Header}>
      {/* logo */}
      <div className={c.logo}>
        <img src={logo} alt='Monash University' />
      </div>

      {/* main header content */}
      <div className={c.mainHeaderContent}>
        <div className={c.left}>
          <h1>
            The digital M-PASS has arrived on iPhone and Apple Watch. Android
            coming soon.
          </h1>

          <p>
            The new digital MPass is available to all enrolled students and
            current staff. Tap your iPhone or Apple Watch at readers across
            campus to access.
          </p>

          <div className={c.cta}>
            <button primary=''>Set up Now</button>
            <a href={'video'}>Watch the 5 min video</a>
          </div>
        </div>

        <div className={c.right}>
          <img className={c.phone} src={phone} alt='iPhone' />
          <img className={c.watch} src={watch} alt='Apple watch' />
        </div>
      </div>

      {/* sub header content */}
      <div className={c.subHeaderContent}>
        <h2>Quick. Easy. Secure.</h2>
        <p>
          Monash prides itself on the early adoption and development of new
          technology and are always working to improve the experience of
          students and staff. We're proud to have developed a new, more
          efficient way for students and staff to access their campus
          facilities.{' '}
        </p>
      </div>

      {/* reference image */}
      <div className={c.referenceImage}>
        <img src={reference} alt='reference' />
      </div>

      {/* blocks */}
      <div className={c.blocks}>
        <h2>Same M-Pass, now digital</h2>

        <div className={c.container}>
          <div>
            <img className={c.blockImage} src={block1} alt='block 1' />
            <div className={c.blockContent}>
              <h3>Available on your Apple devices</h3>
              <p>
                Digital M-Pass is now available on Apple devices (Android coming
                soon). Use your phone or watch wallet to access buildings and
                print throughout all Australian Monash campuses.
              </p>
            </div>
            <img className={c.strip} src={pink} alt='strip' />
          </div>

          <div>
            <img className={c.blockImage} src={block2} alt='block 2' />
            <div className={c.blockContent}>
              <h3>Easy Access</h3>
              <p>
                No need to carry your physical ID card anymore and you'll never
                forget your card again. Easily accessed through your iphone and
                watch.
              </p>
            </div>
            <img className={c.strip} src={green} alt='strip' />
          </div>

          <div>
            <img className={c.blockImage} src={block3} alt='block 3' />
            <div className={c.blockContent}>
              <h3>Top up your M-Pass</h3>
              <p>
                Now make all payments in libraries and other payment facilities
                through your digital M-Pass. Use your Monash app to top and
                access your digital cards.
              </p>
            </div>
            <img className={c.strip} src={purple} alt='strip' />
          </div>
        </div>
      </div>
    </div>
  )
}

export default Header
