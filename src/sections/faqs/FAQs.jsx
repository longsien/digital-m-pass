import Accordion from 'components/accordion/Accordion'
import React from 'react'
import c from './faqs.module.scss'

const FAQs = () => {
  // render

  return (
    <div className={c.FAQs}>
      <div className={c.container}>
        <h2>FAQs</h2>
        <h3>
          Got a question? Search our FAQ's for answers to the most common
          questions, or use Monash Connect if you are still unsure.
        </h3>

        {/* accordions */}
        <div className={c.accordions}>
          <Accordion title={'Getting an M-Pass'}>
            <p>
              If you are enrolling at Monash, M-Pass is now digital and is
              registered under your student email id. You will receive
              information on how to activate an M-Pass. If you haven't received
              your new M-Pass within 10 business days, contact Monash Connect
              online.
            </p>
          </Accordion>

          <Accordion title={'Lost or damaged phone'}>
            <p>
              If you've lost your device contact Monash Connect and we will help
              deactivate your M-Pass for you. You might also want to read
              instructions about Lost/Found Apple devices and Find My iPhone.
            </p>
          </Accordion>

          <Accordion title={'Deactivate the physical M-Pass'}>
            <p>
              Your physical MPass card will be deactivated automatically once
              you set up your new Digital M-Pass. Unfortunately, you can't have
              both.
            </p>
          </Accordion>

          <Accordion title={'Print documents'}>
            <p>
              You can use your digital MPass at all printers across Australian
              campuses, just as you would use your physical M-Pass today"
            </p>
          </Accordion>

          <Accordion title={'View the terms and conditions of use'}>
            <p>
              You can view the full terms and conditions for using the M-Pass.
            </p>
          </Accordion>

          <Accordion title={'Add credit to your account'}>
            <p>
              You can add credit to your account at any time. Use your Monash
              app or the Monash website to top up.
            </p>
          </Accordion>
        </div>
      </div>
    </div>
  )
}

export default FAQs
