import React from 'react'
import c from './getting-started.module.scss'
import phone from 'img/flat-phone.png'
import video from 'img/video.jpg'

const GettingStarted = () => {
  // render

  return (
    <div className={c.GettingStarted}>
      <div className={c.container}>
        <h2>Getting Started</h2>
        <p>
          Minimal setup is required to use your digital M-Pass. Most users can
          begin in minutes. An approved photo is required.
        </p>

        <div className={c.buttons}>
          <button>iOS</button>
          <button secondary=''>Android</button>
        </div>

        {/* content */}
        <div className={c.content}>
          <div className={c.phone}>
            <img src={phone} alt='phone' />
          </div>

          <div className={c.guide}>
            <ul>
              <li>
                <span>1</span>Start
              </li>
              <li>
                <span>2</span>Verify your details through OKTA
              </li>
              <li>
                <span>3</span>Confirm your personal information is correct
              </li>
              <li>
                <span>4</span>Add M-Pass to your digitlal wallet
              </li>
              <li>
                <span>5</span>Use your digitally-enabled wallet to access campus
                buildings, printingfacilities and more.
              </li>
              <li>
                <span className={c.empty}></span>
                <button secondary=''>Begin Registration</button>
              </li>
            </ul>
          </div>
        </div>

        {/* video */}
        <div className={c.video}>
          <h2>Setting up your digital m-pass</h2>
          <h3>Watch this video to learn how to set-up this digital M-Pass</h3>
          <img src={video} alt='video' />
        </div>
      </div>
    </div>
  )
}

export default GettingStarted
