import FAQs from 'sections/faqs/FAQs'
import GettingStarted from 'sections/getting-started/GettingStarted'
import Header from 'sections/header/Header'
import './app.scss'

function App() {
  return (
    <main className='App'>
      <Header />
      <GettingStarted />
      <FAQs />
    </main>
  )
}

export default App
