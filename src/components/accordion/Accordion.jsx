import React, { useRef, useState } from 'react'
import c from './accordion.module.scss'
import chevron from 'img/chevron.png'

const Accordion = ({ title, children }) => {
  const [active, setActive] = useState(false)
  const [height, setHeight] = useState(0)

  const ref = useRef()

  const toggle = () => {
    if (active) {
      setHeight(0)
      setActive(false)
    } else {
      setHeight(ref.current.scrollHeight)
      setActive(true)
    }
  }

  // render

  return (
    <div className={c.Accordion}>
      <div className={c.title} onClick={toggle} active={active ? '' : null}>
        <h3>{title}</h3>
        <img src={chevron} alt='chevron' />
      </div>
      <div className={c.content} style={{ height: `${height}px` }}>
        <div className={c.children} ref={ref}>
          {children}
        </div>
      </div>
    </div>
  )
}

export default Accordion
